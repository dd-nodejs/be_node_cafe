const { request, response } = require('express');
const { Product } = require('../models');

const getProducts = async (req = request, res = response) => {
  try {
    const { limit, active = true, from = 0 } = req.query;

    const query = {
      active: typeof Boolean(active) === 'boolean' ? active : false
    };

    const [total, products] = await Promise.all([
      Product.countDocuments(query),
      Product.find(query)
        .populate('user', 'name email')
        .populate('category', 'name')
        .skip(isNaN(Number(from)) ? 0 : Number(from))
        .limit(isNaN(Number(limit)) ? 0 : Number(limit))
        .sort({ createdAt: 'ascending' })
    ]);

    res.json({ total, products });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `products get all`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'products',
          location: 'products get all'
        }
      ]
    });
  }
};

const getProductByID = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const product = await Product.findOne({
      _id: id,
      active: true
    })
      .populate('user', 'name')
      .populate('category', 'name');
    res.json(product);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `product get by ID`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'product',
          location: 'product get by ID'
        }
      ]
    });
  }
};

const createProduct = async (req = request, res = response) => {
  try {
    const { name, active, user, ...rest } = req.body;
    const { _id } = req.authenticated_user;

    const data = {
      active: true,
      name: name.toUpperCase(),
      user: _id,
      ...rest
    };

    const product = new Product(data);
    await product.save();

    return res.status(201).json(product);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: [
        {
          value: `product create`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'product',
          location: 'product create'
        }
      ]
    });
  }
};

const updateProduct = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const { _id, active, user, ...data } = req.body;

    data.name = data.name ? data.name.toUpperCase() : undefined;
    data.user = req.authenticated_user._id;

    const product = await Product.findByIdAndUpdate(id, data, {
      validateBeforeSave: true,
      returnOriginal: false
    });

    res.json(product);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `product update`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'product',
          location: 'product update'
        }
      ]
    });
  }
};

const deleteProduct = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const product = await Product.findByIdAndUpdate(
      id,
      {
        active: false
      },
      {
        validateBeforeSave: true,
        returnOriginal: false
      }
    );

    res.json(product);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `product delete`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'product',
          location: 'product delete'
        }
      ]
    });
  }
};

module.exports = {
  getProducts,
  getProductByID,
  createProduct,
  updateProduct,
  deleteProduct
};
