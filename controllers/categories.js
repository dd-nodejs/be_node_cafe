const { request, response } = require('express');
const { Category } = require('../models');

const getCategories = async (req = request, res = response) => {
  try {
    const { limit, active = true, from = 0 } = req.query;

    const query = {
      active: typeof Boolean(active) === 'boolean' ? active : false
    };

    const [total, categories] = await Promise.all([
      Category.countDocuments(query),
      Category.find(query)
        .populate('user', 'name email')
        .skip(isNaN(Number(from)) ? 0 : Number(from))
        .limit(isNaN(Number(limit)) ? 0 : Number(limit))
        .sort({ createdAt: 'ascending' })
    ]);

    res.json({ total, categories });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `category get all`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'category',
          location: 'category get all'
        }
      ]
    });
  }
};

const getCategoryByID = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const category = await Category.findOne({
      _id: id,
      active: true
    }).populate('user', 'name email');

    res.json(category);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `category find by ID`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'category',
          location: 'category find by ID'
        }
      ]
    });
  }
};

const createCategory = async (req = request, res = response) => {
  try {
    const name = req.body.name.toUpperCase();
    const { _id } = req.authenticated_user;
    const data = { active: true, name, user: _id };

    const category = new Category(data);
    await category.save();

    res.status(201).json(category);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: [
        {
          value: `category create`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'category',
          location: 'category create'
        }
      ]
    });
  }
};

const updateCategory = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const { _id, active, user, ...data } = req.body;

    data.name = data.name.toUpperCase();
    data.user = req.authenticated_user._id;

    const category = await Category.findByIdAndUpdate(id, data, {
      validateBeforeSave: true,
      returnOriginal: false
    });

    res.json(category);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: [
        {
          value: `category update`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'category',
          location: 'category update'
        }
      ]
    });
  }
};

const deleteCategory = async (req = request, res = response) => {
  try {
    const { id } = req.params;

    const category = await Category.findByIdAndUpdate(
      id,
      {
        active: false
      },
      {
        validateBeforeSave: true,
        returnOriginal: false
      }
    );

    res.json(category);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: [
        {
          value: `category delete`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'category',
          location: 'category delete'
        }
      ]
    });
  }
};

module.exports = {
  getCategories,
  getCategoryByID,
  createCategory,
  updateCategory,
  deleteCategory
};
