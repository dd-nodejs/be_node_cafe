const authController = require('../controllers/auth');
const categoryController = require('./categories');
const productController = require('./products');
const searchController = require('./search');
const uploadsController = require('./uploads');
const userController = require('../controllers/users');

module.exports = {
  ...authController,
  ...categoryController,
  ...productController,
  ...searchController,
  ...uploadsController,
  ...userController
};
