const { request, response } = require('express');
const { isValidObjectId } = require('mongoose');
const { Category, Product, User } = require('../models');

const allowedCollections = ['categories', 'products', 'role', 'users'];

const searchUsers = async (term = '', res = response) => {
  try {
    const isMongoID = isValidObjectId(term);

    if (isMongoID) {
      const user = await User.findById(term);
      return res.json({
        results: user ? [user] : []
      });
    }

    const regex = new RegExp(term, 'i');

    const users = await User.find({
      // $text: { $search: regex }
      $or: [{ name: regex }, { email: regex }],
      $and: [{ active: true }]
    }).sort({ createdAt: 'ascending' });

    res.json({
      results: users
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `search users`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'search',
          location: 'search users'
        }
      ]
    });
  }
};

const searchCategories = async (term = '', res = response) => {
  try {
    const isMongoID = isValidObjectId(term);

    if (isMongoID) {
      const category = await Category.findById(term).populate('user', 'name');
      return res.json({ results: category ? [category] : [] });
    }

    const regex = new RegExp(term, 'i');

    const categories = await Category.find({
      // $text: { $search: regex }
      $or: [{ name: regex }],
      $and: [{ active: true }]
    })
      .populate('user', 'name')
      .sort({ createdAt: 'ascending' });

    res.json({
      results: categories
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `search categories`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'search',
          location: 'search categories'
        }
      ]
    });
  }
};

const searchProducts = async (term = '', res = response) => {
  try {
    const isMongoID = isValidObjectId(term);

    if (isMongoID) {
      const product = await Product.findById(term)
        .populate('user', 'name')
        .populate('category', 'name');
      return res.json({ results: product ? [product] : [] });
    }

    const regex = new RegExp(term, 'i');

    const products = await Product.find({
      // $text: { $search: regex }
      $or: [{ name: regex }],
      $and: [{ active: true }]
    })
      .populate('user', 'name')
      .populate('category', 'name')
      .sort({ createdAt: 'ascending' });

    res.json({
      results: products
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `search products`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'search',
          location: 'search products'
        }
      ]
    });
  }
};

const search = async (req = request, res = response) => {
  try {
    const { collection, term } = req.params;

    if (!allowedCollections.includes(collection)) {
      res.status(400).json({
        errors: [
          {
            value: `${collection}`,
            msg: `La coleccion ${collection} no está permitida`,
            param: 'search',
            location: 'search collection'
          }
        ]
      });
    }

    switch (collection) {
      case 'categories':
        searchCategories(term, res);
        break;
      case 'products':
        searchProducts(term, res);
        break;
      case 'users':
        searchUsers(term, res);
        break;
      default:
        res.status(404).json({
          msg: 'COLLECTION NOT HANDLED'
        });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      errors: [
        {
          value: `search all`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'search',
          location: 'search all'
        }
      ]
    });
  }
};

module.exports = {
  search
};
