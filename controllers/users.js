const { request, response } = require('express');
const bcrypt = require('bcryptjs');

const User = require('../models/user');

const userIndex = async (req = request, res = response) => {
  const { limit, active = true, from = 0 } = req.query;

  /* const users = await User.find({ active })
    .select('name email role active google -_id')
    .skip(isNaN(Number(from)) ? 0 : Number(from))
    .limit(isNaN(Number(limit)) ? 0 : Number(limit))
    .lean()
    .sort({ createdAt: 'ascending' });

  const total = await User.countDocuments({ active }); */

  const query = {
    active: typeof Boolean(active) === 'boolean' ? active : false
  };

  const [total, users] = await Promise.all([
    User.countDocuments(query),
    User.find(query)
      .skip(isNaN(Number(from)) ? 0 : Number(from))
      .limit(isNaN(Number(limit)) ? 0 : Number(limit))
      .sort({ createdAt: 'ascending' })
  ]);

  res.json({
    total,
    users
  });
};

const userCreate = async (req = request, res = response) => {
  const { name, email, password, img = undefined, role = 'USER_ROLE' } = req.body;

  const user = new User({
    active: true,
    email: email.toLowerCase(),
    google: false,
    img,
    name,
    password: bcrypt.hashSync(password),
    role
  });

  await user.save({ validateBeforeSave: true });

  res.json(user);
};

const userUpdate = async (req = request, res = response) => {
  const { id } = req.params;

  const { _id, password, google, ...rest } = req.body;

  const user = await User.findByIdAndUpdate(id, rest, {
    validateBeforeSave: true,
    returnOriginal: false
  });

  res.json(user);
};

const userDelete = async (req = request, res = response) => {
  const { id } = req.params;

  // Physical delete (remove document from db)
  // const user = await User.findByIdAndDelete(id);

  // Logic delete
  const user = await User.findByIdAndUpdate(
    id,
    {
      active: false
    },
    {
      validateBeforeSave: true,
      returnOriginal: false
    }
  );

  res.json(user);
};

const userPatch = (req = request, res = response) => {
  res.json({
    data: 'PATCH api - controller'
  });
};

module.exports = {
  userIndex,
  userCreate,
  userUpdate,
  userDelete,
  userPatch
};
