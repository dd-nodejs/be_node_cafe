const path = require('path');
const fs = require('fs');
const { request, response } = require('express');
const cloudinary = require('cloudinary').v2
cloudinary.config(process.env.CLOUDINARY_URL);

const { uploadFileInPath } = require('../helpers');
const { Product, User } = require('../models');

const uploadFile = async (req = request, res = response) => {
  try {
    const file = await uploadFileInPath(req.files);

    res.json({ file });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }
};

const updateCollectionImage = async (req = request, res = response) => {
  try {
    const { collection, id } = req.params;

    let model;

    switch (collection) {
      case 'users':
        model = await User.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload user image`,
                msg: `No existe un usuario con el ID ${id}`,
                param: 'id',
                location: 'upload upload user image'
              }
            ]
          });
        }
        break;
      case 'products':
        model = await Product.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload product image`,
                msg: `No existe un producto con el ID ${id}`,
                param: 'id',
                location: 'upload upload product image'
              }
            ]
          });
        }
        break;

      default:
        return res.status(404).json({ msg: 'COLLECTION NOT HANDLED' });
    }

    // Replace user image
    if (collection === 'users' && model.img) {
      // Delete img from server
      const imagePath = path.join(__dirname, '../uploads/users/', model.img);

      if (fs.existsSync(imagePath)) {
        fs.unlinkSync(imagePath);
      }
    }

    const fileName = await uploadFileInPath(req.files, collection);

    collection === 'products'
      ? (model.images = [...model.images, fileName])
      : (model.img = fileName);

    await model.save();

    res.json(model);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }
};

const updateCollectionImageCloudinary = async (req = request, res = response) => {
  try {
    const { collection, id } = req.params;

    let model;

    switch (collection) {
      case 'users':
        model = await User.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload user image`,
                msg: `No existe un usuario con el ID ${id}`,
                param: 'id',
                location: 'upload upload user image'
              }
            ]
          });
        }
        break;
      case 'products':
        model = await Product.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload product image`,
                msg: `No existe un producto con el ID ${id}`,
                param: 'id',
                location: 'upload upload product image'
              }
            ]
          });
        }
        break;

      default:
        return res.status(404).json({ msg: 'COLLECTION NOT HANDLED' });
    }

    // Replace user image
    if (collection === 'users' && model.img) {
      // Delete img from cloudinary
      const nameArr = model.img.split('/');
      const name = nameArr[nameArr.length - 1];
      const [public_id] = name.split('.');
      cloudinary.uploader.destroy(public_id);
    }

    const { tempFilePath } = req.files.file;

    const { secure_url } = await cloudinary.uploader.upload(tempFilePath);

    collection === 'products'
      ? (model.images = [...model.images, secure_url])
      : (model.img = secure_url);

    await model.save();

    res.json(model);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }
};

const deleteProductImage = async (req = request, res = response) => {
  try {
    const { id } = req.params;
    const { fileName } = req.body;

    const product = await Product.findById(id);

    if (!product.images.includes(fileName)) {
      return res.status(400).json({
        errors: [
          {
            value: `upload file`,
            msg: `El archivo ${fileName} no existe en este producto`,
            param: 'uploads',
            location: 'upload file'
          }
        ]
      });
    }

    // Delete img from server
    const imagePath = path.join(__dirname, '../uploads/products/', fileName);
    if (fs.existsSync(imagePath)) {
      fs.unlinkSync(imagePath);
    }

    // Filter images to exclude the requested to delete
    const newImages = product.images.filter(img => img !== fileName);

    product.images = newImages;
    await product.save();

    res.json(product);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }
};

const deleteProductImageCloudinary = async (req = request, res = response) => {
  try {
    const { id } = req.params;
    const { fileName } = req.body;

    const product = await Product.findById(id);

    if (!product.images.includes(fileName)) {
      return res.status(400).json({
        errors: [
          {
            value: `upload file`,
            msg: `El archivo ${fileName} no existe en este producto`,
            param: 'uploads',
            location: 'upload file'
          }
        ]
      });
    }

    // Delete img from cloudinary
    const nameArr = fileName.split('/');
    const name = nameArr[nameArr.length - 1];
    const [public_id] = name.split('.');
    console.log(name);
    cloudinary.uploader.destroy(public_id);

    const imagePath = path.join(__dirname, '../uploads/products/', fileName);
    if (fs.existsSync(imagePath)) {
      fs.unlinkSync(imagePath);
    }

    // Filter images to exclude the requested to delete
    const newImages = product.images.filter(img => img !== fileName);

    product.images = newImages;
    await product.save();

    res.json(product);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }
};

const showImages = async (req = request, res = response) => {

  try {
    const { collection, id } = req.params;

    let model;

    switch (collection) {
      case 'users':
        model = await User.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload user image`,
                msg: `No existe un usuario con el ID ${id}`,
                param: 'id',
                location: 'upload upload user image'
              }
            ]
          });
        }
        break;
      case 'products':
        model = await Product.findById(id);
        if (!model) {
          return res.status(401).json({
            errors: [
              {
                value: `upload product image`,
                msg: `No existe un producto con el ID ${id}`,
                param: 'id',
                location: 'upload upload product image'
              }
            ]
          });
        }
        break;

      default:
        return res.status(404).json({ msg: 'COLLECTION NOT HANDLED' });
    }

    if (collection === 'users' && model.img) {
      const imagePath = path.join(__dirname, '../uploads/users/', model.img);

      if (fs.existsSync(imagePath)) {
        return res.sendFile(imagePath)
      }
    }

    if (collection === 'products' && model.images.length) {
      const imagePath = path.join(__dirname, '../uploads/products/', model.images[0]);

      if (fs.existsSync(imagePath)) {
        return res.sendFile(imagePath)
      }
    }

    const imagePath = path.join(__dirname, '../assets/no-image.jpg');

    res.sendFile(imagePath);

  } catch (error) {
    console.log(error);
    res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: error,
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }

};

module.exports = {
  uploadFile,
  updateCollectionImage,
  deleteProductImage,
  showImages,
  updateCollectionImageCloudinary,
  deleteProductImageCloudinary
};
