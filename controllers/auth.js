const { request, response } = require('express');
const bcrypt = require('bcryptjs');

const User = require('../models/user');
const { generateJWT, verifyGoogleToken } = require('../helpers');

const handleLogin = async (req = request, res = response) => {
  const { email, password } = req.body;

  try {
    // TODO: Verify is user exist
    const user = await User.findOne({ email });

    if (!user) {
      res.status(401).json({
        errors: [
          {
            value: `Correo electrónico ${email}, contraseña ${password}`,
            msg: 'No existen coincidencias con las credenciales enviadas',
            param: 'login',
            location: 'login'
          }
        ]
      });
    }

    // TODO: Verify is user is active
    if (!user.active) {
      res.status(401).json({
        errors: [
          {
            value: `Correo electrónico ${email}, contraseña ${password}`,
            msg: 'No existen coincidencias con las credenciales enviadas',
            param: 'login',
            location: 'login'
          }
        ]
      });
    }

    // TODO: Verify is user password match
    const validPassword = bcrypt.compareSync(password, user.password);
    if (!validPassword) {
      res.status(401).json({
        errors: [
          {
            value: `Correo electrónico ${email}, contraseña ${password}`,
            msg: 'No existen coincidencias con las credenciales enviadas',
            param: 'login',
            location: 'login'
          }
        ]
      });
    }

    // Generate JWT
    const jwt = await generateJWT(user.id, user.email);

    res.json({
      user,
      jwt
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: [
        {
          value: 'internal error',
          msg: 'Lo sentimos ocurrio un error en el servidor',
          param: 'server',
          location: 'server'
        }
      ]
    });
  }
};

const googleSignIn = async (req = request, res = response) => {
  const { id_token } = req.body;

  try {
    const { email, name, img } = await verifyGoogleToken(id_token);

    let user = await User.findOne({ email });

    // create user if does not exist
    if (!user) {
      const data = {
        active: true,
        email,
        google: true,
        img,
        name,
        password: '>_<',
        role: 'USER_ROLE'
      };

      user = new User(data);
      await user.save();
    }

    // Check if user is active
    if (!user.active) {
      return res.status(401).json({
        errors: [
          {
            value: `User deleted`,
            msg: 'No existen coincidencias con las credenciales enviadas',
            param: 'Google sign in',
            location: 'Google sign in'
          }
        ]
      });
    }

    // Generate JWT
    const jwt = await generateJWT(user.id, user.email);

    res.json({ user, jwt });
  } catch (error) {
    console.error(error);
    return res.status(400).json({
      errors: [
        {
          value: 'internal error',
          msg: 'Lo sentimos ocurrio un error en el servidor',
          param: 'server',
          location: 'server google verify'
        }
      ]
    });
  }
};

module.exports = {
  handleLogin,
  googleSignIn
};
