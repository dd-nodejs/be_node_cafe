const { Schema, model, models } = require('mongoose');

const categorySchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'El nombre de la categoría es requerido']
    },
    active: {
      type: Boolean,
      default: true,
      required: [true, 'El estado de la categoría es requerido']
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    }
  },
  {
    timestamps: true
  }
);

// Replace toJSON method to skip model fields (avoid using arrow function for maintaint right scope of 'this')
categorySchema.methods.toJSON = function() {
  const { __v, createdAt, updatedAt, _id: uid, ...rest } = this.toObject();

  return {
    uid,
    ...rest
  };
};

const Category = models.Category || model('Category', categorySchema);

module.exports = Category;
