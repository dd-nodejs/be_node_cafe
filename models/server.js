const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const { dbConnection } = require('../database/config');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;

    // Connect to database
    this.connectDB();
    // Middlewares
    this.middlewares();
    // Inicializar rutas
    this.routes();
  }

  async connectDB() {
    await dbConnection();
  }

  middlewares() {
    // Expose public directory
    this.app.use(express.static('public'));

    // Lectura y parseo del body
    this.app.use(express.json());

    // CORS
    this.app.use(cors());

    // File uploads
    this.app.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: '/tmp/',
        createParentPath: true
      })
    );
  }

  routes() {
    // Auth routes
    this.app.use('/api/auth', require('../routes/auth'));

    // Seed Users
    this.app.use('/api/seed', require('../routes/seed'));

    // Users routes
    this.app.use('/api/users', require('../routes/users'));

    // Categories routes
    this.app.use('/api/categories', require('../routes/categories'));

    // Products routes
    this.app.use('/api/products', require('../routes/products'));

    // Search routes
    this.app.use('/api/search', require('../routes/search'));

    // Uploads routes
    this.app.use('/api/uploads', require('../routes/uploads'));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`App runnin' at port ${this.port}`);
    });
  }
}

module.exports = Server;
