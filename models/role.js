const { Schema, model, models } = require('mongoose');

const roleSchema = new Schema(
  {
    role: {
      type: String,
      enum: {
        values: ['ADMIN_ROLE', 'USER_ROLE', 'SALES_ROLE'],
        message: 'El rol {VALUE} no es válido',
        default: 'USER_ROLE',
        required: true
      }
    }
  },
  {
    timestamps: true
  }
);

const Role = models.Role || model('Role', roleSchema);

module.exports = Role;
