const { Schema, model, models } = require('mongoose');

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'El nombre del usuario es obligatorio']
    },
    email: {
      type: String,
      required: [true, 'El correo electrónico es obligagorio'],
      unique: true
    },
    password: {
      type: String,
      required: [true, 'La contraseña es oblogatoria']
    },
    img: { type: String },
    role: {
      type: String,
      enum: {
        values: ['ADMIN_ROLE', 'USER_ROLE'],
        message: 'El rol {VALUE} no es válido',
        default: 'USER_ROLE',
        required: true
      }
    },
    active: {
      type: Boolean,
      default: true
    },
    google: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
);

// Replace toJSON method to skip model fields (avoid using arrow function for maintaint right scope of 'this')
userSchema.methods.toJSON = function() {
  const {
    __v,
    password,
    createdAt,
    updatedAt,
    _id: uid,
    ...rest
  } = this.toObject();

  return {
    uid,
    ...rest
  };
};

userSchema.index({ name: 'text', email: 'text' });

const User = models.User || model('User', userSchema);

module.exports = User;
