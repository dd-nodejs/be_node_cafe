const { Schema, model, models } = require('mongoose');

const productSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'El nombre de la categoría es requerido']
    },
    active: {
      type: Boolean,
      default: true,
      required: [true, 'El estado de la categoría es requerido']
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    price: {
      type: Number,
      default: 0
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: true
    },
    description: {
      type: String
    },
    aviable: {
      type: Boolean
    },
    inStock: { type: Number, required: true, default: 0 },
    images: [{ type: String }]
  },
  {
    timestamps: true
  }
);

// Replace toJSON method to skip model fields (avoid using arrow function for maintaint right scope of 'this')
productSchema.methods.toJSON = function() {
  const { __v, createdAt, updatedAt, _id: uid, ...rest } = this.toObject();

  return {
    uid,
    ...rest
  };
};

productSchema.index({ name: 'text', description: 'text' });

const Product = models.Product || model('Product', productSchema);

module.exports = Product;
