const bcrypt = require('bcryptjs');

const initialData = {
  roles: [
    {
      role: 'ADMIN_ROLE'
    },
    {
      role: 'USER_ROLE'
    },
    {
      role: 'SALES_ROLE'
    }
  ],
  users: [
    {
      name: 'Diego Dom',
      email: 'ddominguez@dev.com',
      role: 'ADMIN_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Sean Rafd',
      email: 'rafsean@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Carl Greens',
      email: 'cargreennsa@example.com',
      role: 'USER_ROLE',
      active: false,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'John Doe',
      email: 'johndoe@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Sam Dee',
      email: 'samdee@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Chris Adams',
      email: 'adamschris@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Hanrmon Juds',
      email: 'harnjudbs@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Aliyaah Dom',
      email: 'domaliyaah@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Noid Stuart',
      email: 'stuunod@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Arthur Sam',
      email: 'mrsamyo@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Jacob Linc',
      email: 'lincjack@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Adan Smith',
      email: 'isadamsmith@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Tenna Smith',
      email: 'tennsmith@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Al Crisn',
      email: 'mccrisn@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    },
    {
      name: 'Ann Doe',
      email: 'lilanndoe@example.com',
      role: 'USER_ROLE',
      active: true,
      google: false,
      password: bcrypt.hashSync('qwerty')
    }
  ]
};

module.exports = initialData;
