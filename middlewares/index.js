const validateFields = require('./validate-fields');
const validateJWT = require('./validate-jwt');
const validateRoles = require('./validate-role');
const validateUploads = require('./validate-uploads');

module.exports = {
  ...validateFields,
  ...validateJWT,
  ...validateRoles,
  ...validateUploads
};
