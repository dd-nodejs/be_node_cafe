const { request, response } = require('express');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const validateJWT = async (req = request, res = response, next) => {
  const token = req.header('x-token');

  if (!token) {
    return res.status(401).json({
      errors: [
        {
          value: `Unauthorized`,
          msg: 'No está autorizado para acceder a este recurso',
          param: 'authorize',
          location: 'authorize'
        }
      ]
    });
  }

  const secret = process.env.JWT_SECRET_SEED;

  if (!secret) {
    return res.status(500).json({
      errors: [
        {
          value: `Unauthorized`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'authorize',
          location: 'authorize'
        }
      ]
    });
  }

  try {
    const { uid } = jwt.verify(token, secret);

    const user = await User.findById(uid);
    req.authenticated_user = user;

    // Verify that user exist
    if (!user) {
      throw new Error('Unauthorized');
    }

    // Verify if user is active
    if (!user.active) {
      throw new Error('Unauthorized');
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({
      errors: [
        {
          value: `Unauthorized`,
          msg: 'No está autorizado para acceder a este recurso',
          param: 'authorize',
          location: 'authorize'
        }
      ]
    });
  }
};

module.exports = {
  validateJWT
};
