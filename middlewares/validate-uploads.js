const { request, response } = require('express');

const validateUpdaloadFile = (req = request, res = response, next) => {
  if (!req.files || Object.keys(req.files).length === 0 || !req.files.file) {
    return res.status(400).json({
      errors: [
        {
          value: `upload file`,
          msg: 'Se debe enviar [file] en la petición',
          param: 'uploads',
          location: 'upload file'
        }
      ]
    });
  }

  next();
};

module.exports = {
  validateUpdaloadFile
};
