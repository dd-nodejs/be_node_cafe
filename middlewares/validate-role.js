const { request, response } = require('express');

const isAdminRole = (req = request, res = response, next) => {
  const authenticated_user = req.authenticated_user;

  if (!authenticated_user) {
    return res.status(500).json({
      errors: [
        {
          value: `Validate role`,
          msg: 'Ocurrio un error inesperado en el servidor',
          param: 'user',
          location: 'Validate role'
        }
      ]
    });
  }

  const { role, name } = authenticated_user;

  if (role !== 'ADMIN_ROLE') {
    return res.status(401).json({
      errors: [
        {
          value: `Unauthorized`,
          msg: `No ${name} está autorizado para realizar esta acción`,
          param: 'authorize',
          location: 'Try to validate role before jwt'
        }
      ]
    });
  }

  next();
};

const hasRole = (...roles) => {
  return (req = request, res = response, next) => {
    const authenticated_user = req.authenticated_user;

    if (!authenticated_user) {
      return res.status(500).json({
        errors: [
          {
            value: `Validate role`,
            msg: 'Ocurrio un error inesperado en el servidor',
            param: 'user',
            location: 'Try to validate role before jwt'
          }
        ]
      });
    }

    const { role, name } = authenticated_user;

    if (!roles.includes(role)) {
      return res.status(401).json({
        errors: [
          {
            value: `Unauthorized`,
            msg: `No ${name} está autorizado para realizar esta acción`,
            param: 'authorize',
            location: 'authorize'
          }
        ]
      });
    }

    next();
  };
};

module.exports = {
  isAdminRole,
  hasRole
};
