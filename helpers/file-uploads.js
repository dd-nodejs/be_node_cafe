const path = require('path');
const { v4: uuidv4 } = require('uuid');

const uploadFileInPath = (
  files,
  folder = '',
  allowedExtensions = ['png', 'jpg', 'jpeg', 'gif']
) => {
  return new Promise((resolve, reject) => {
    const { file } = files;
    const cuttedName = file.name.split('.');

    const extension = cuttedName[cuttedName.length - 1];

    // Validate file extension agaist allowed extensions
    if (!allowedExtensions.includes(extension)) {
      return reject(
        `Se debe enviar un archivo con la extensión ${allowedExtensions}`
      );
    }

    const tempName = `${uuidv4()}.${extension}`;

    const uploadPath = path.join(__dirname, '../uploads/', folder, tempName);

    file.mv(uploadPath, err => {
      if (err) {
        console.log(err);

        return reject('Ocurrio un error inesperado en el servidor');
      }

      resolve(tempName);
    });
  });
};

module.exports = {
  uploadFileInPath
};
