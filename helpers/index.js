const dbValidators = require('./db-validators');
const fileUploads = require('./file-uploads');
const googleVerify = require('./google-verify');
const jwt = require('./jwt');

module.exports = {
  ...dbValidators,
  ...fileUploads,
  ...googleVerify,
  ...jwt
};
