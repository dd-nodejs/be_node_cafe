const { request } = require('express');
const { isValidObjectId } = require('mongoose');

const { Category, Role, User, Product } = require('../models');

const validateRole = async (role = '') => {
  const dbRole = await Role.findOne({ role });
  if (!dbRole) {
    throw new Error(`${role} no es un rol válido`);
  }
};

const validateUniqueEmail = async (email = '') => {
  const user = await User.findOne({ email: email.toLowerCase() }).lean();

  if (user) {
    throw new Error(`El correo electrónico ${email} ya está registrado`);
  }
};

const validateUserExistByID = async (id = '') => {
  const user = await User.findById(id).lean();
  if (!user) {
    throw new Error(`No existe ningun usuario con el id ${id}`);
  }
};

const validateUserEmailChange = async (email, req = request) => {
  const { id } = req.params;

  const user = await User.findOne({ email: email.toLowerCase() }).lean();

  if (user) {
    if (user._id.toString() !== id) {
      throw new Error(`El correo electrónico ${email} ya está registrado`);
    }
  }
};

const validateCategoryExistByID = async (id = '') => {
  const category = await Category.findById(id).lean();
  if (!category) {
    throw new Error(`No existe ninguna categoria con el id ${id}`);
  }
};

const validateUniqueCategory = async (name = '') => {
  const category = await Category.findOne({
    name: name.toUpperCase(),
    active: true
  }).lean();

  if (category) {
    throw new Error(`La categoría ${name} ya está registrada`);
  }
};

const validateProductExistByID = async (id = '') => {
  const product = await Product.findById(id).lean();
  if (!product) {
    throw new Error(`No existe ningun producto con el id ${id}`);
  }
};

const validateUniqueProduct = async (name = '') => {
  const product = await Product.findOne({
    name: name.toUpperCase(),
    active: true
  }).lean();

  if (product) {
    throw new Error(`El producto ${name} ya está registrado`);
  }
};

module.exports = {
  validateRole,
  validateUniqueEmail,
  validateUserExistByID,
  validateUserEmailChange,
  validateCategoryExistByID,
  validateUniqueCategory,
  validateProductExistByID,
  validateUniqueProduct
};
