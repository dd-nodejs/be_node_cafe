const jwt = require('jsonwebtoken');

const generateJWT = (uid = '', email = '') => {
  const secret = process.env.JWT_SECRET_SEED;

  if (!secret) {
    throw new Error('No hay semilla de JWT - Revisar variables de entorno');
  }

  return new Promise((resolve, reject) => {
    try {
      const payload = { uid, email };

      jwt.sign(payload, secret, { expiresIn: '1d' }, (err, token) => {
        if (err) {
          console.log(err);
          reject('Error al firmar el JWT');
        }

        resolve(token);
      });
    } catch (error) {
      console.log(error);
      reject('Error al firmar el JWT');
    }
  });
};

module.exports = {
  generateJWT
};
