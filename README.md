# Node-Cafe
## Instructions

Install node packages.
```
npm install
```

Copy **.env.example** and rename it as **.env**. You must use this file as a template for the required enviroment variables.
<br/>
- Add PORT variable at .env, to use it as endpoint:

```
PORT=yourPreferedportgoeshere
```

You must have a mongoDB cluster or locally configured connection.
```
MONGODB_CNN=yourmongoDBstrinconnection
```
* MongoDB CNN example using mongoDB locally:
```
mongodb://localhost:27017/teslodb
```

Start nodeJS server.
```
node app.js | nodemon app
```
- ***nodemon*** command only works if you have been installed the package
<br/>


Run the database seeder for test DB connection and API features.
```
http://localhost:PORT/api/seed
```

 