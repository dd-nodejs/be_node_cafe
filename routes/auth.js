const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields } = require('../middlewares');
const { handleLogin, googleSignIn } = require('../controllers');

const router = Router();

router.post(
  '/login',
  [
    check('email', 'El correo electrónico no es válido').isEmail(),
    check('password', 'La contraseña del usuario no es válida (min 6 carácteres)').not().isEmpty(),
    validateFields
  ],
  handleLogin
);

router.post(
  '/google',
  [check('id_token', 'El ID_TOKEN no es válido').not().isEmpty(), validateFields],
  googleSignIn
);

module.exports = router;
