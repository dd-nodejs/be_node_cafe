const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, isAdminRole } = require('../middlewares');
const {
  validateCategoryExistByID,
  validateProductExistByID,
  validateUniqueProduct
} = require('../helpers');
const {
  getProducts,
  createProduct,
  getProductByID,
  updateProduct,
  deleteProduct
} = require('../controllers');

const router = Router();

router.get('/', getProducts);

router.get(
  '/:id',
  [
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateProductExistByID),
    validateFields
  ],
  getProductByID
);

router.post(
  '/',
  [
    validateJWT,
    check('name', 'El nombre del producto no es válido').not().isEmpty(),
    check('name').custom(validateUniqueProduct),
    check('price', 'El precio del producto no es válido')
      .not()
      .isEmpty()
      .isInt({ min: 0 }),
    check('category', 'El id no es válido').isMongoId(),
    check('category').custom(validateCategoryExistByID),
    check('description', 'La descripcion del producto no es válida')
      .not()
      .isEmpty()
      .optional({
        nullable: false
      }),
    check('aviable').isBoolean(),
    check('inStock').not().isEmpty().isInt({ min: 0 }),
    check('images', 'Las imagenes del producto deben ser un arreglo de enlaces')
      .isArray()
      .optional({
        nullable: false
      }),
    validateFields
  ],
  createProduct
);

router.put(
  '/:id',
  [
    validateJWT,
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateProductExistByID),
    check('name', 'El nombre del producto no es válido')
      .not()
      .isEmpty()
      .optional({
        nullable: false
      }),
    check('name').custom(validateUniqueProduct),
    check('price', 'El precio del producto no es válido')
      .not()
      .isEmpty()
      .isInt({ min: 0 })
      .optional({
        nullable: false
      }),
    check('category', 'El id no es válido').isMongoId().optional({
      nullable: false
    }),
    check('category', 'La categoría del producto no es válido')
      .custom(validateCategoryExistByID)
      .optional({
        nullable: false
      }),
    check('description', 'La descripcion del producto no es válida')
      .not()
      .isEmpty()
      .optional({
        nullable: false
      }),
    check('aviable').isBoolean().optional({
      nullable: false
    }),
    check('inStock').not().isEmpty().isInt({ min: 0 }).optional({
      nullable: false
    }),
    check('images', 'Las imagenes del producto deben ser un arreglo de enlaces')
      .isArray()
      .optional({
        nullable: false
      }),
    validateFields
  ],
  updateProduct
);

router.delete(
  '/:id',
  [
    validateJWT,
    isAdminRole,
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateProductExistByID),
    validateFields
  ],
  deleteProduct
);

module.exports = router;
