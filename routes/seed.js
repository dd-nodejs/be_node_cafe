const { Router } = require('express');
const { response } = require('express');
const { check } = require('express-validator');

const User = require('../models/user');
const Role = require('../models/role');
const initialData = require('../database/seed-data');

const router = Router();

router.get('/', async (req, res = response) => {
  if (process.env.NODE_ENV === 'production') {
    return res.status(401).json({
      success: false,
      message: 'Este endpoint es solo para desarrollo'
    });
  }

  await User.deleteMany();
  await User.insertMany(initialData.users);

  await Role.deleteMany();
  await Role.insertMany(initialData.roles);

  res.status(200).json({ success: true, message: 'Proceso del seed realizado correctamente' });
});

module.exports = router;
