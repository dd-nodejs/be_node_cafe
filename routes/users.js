const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, isAdminRole, hasRole } = require('../middlewares');
const {
  validateRole,
  validateUniqueEmail,
  validateUserExistByID,
  validateUserEmailChange
} = require('../helpers');

const { userIndex, userCreate, userUpdate, userDelete } = require('../controllers');

const router = Router();

router.get('/', userIndex);

router.post(
  '/',
  [
    check('name', 'El nombre del usuario no es válido').not().isEmpty(),
    check('email', 'El correo electrónico no es válido').isEmail(),
    check('email').custom(validateUniqueEmail),
    check('password', 'La contraseña del usuario no es válida (min 6 carácteres)')
      .not()
      .isEmpty()
      .isLength({ min: 6 }),
    check('role').custom(validateRole).optional({ nullable: false })
  ],
  validateFields,
  userCreate
);

router.put(
  '/:id',
  [
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateUserExistByID),
    check('name', 'El nombre del usuario no es válido').not().isEmpty().optional({
      nullable: false
    }),
    check('email', 'El correo electrónico no es válido')
      .optional({ nullable: false })
      .isEmail()
      .custom((email, { req }) => validateUserEmailChange(email, req)),
    check('role')
  ],
  validateFields,
  userUpdate
);

router.delete(
  '/:id',
  [
    validateJWT,
    // isAdminRole,
    hasRole('ADMIN_ROLE', 'SALES_ROLE'),
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateUserExistByID)
  ],
  validateFields,
  userDelete
);

module.exports = router;
