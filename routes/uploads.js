const { Router } = require('express');
const { check } = require('express-validator');
const {
  uploadFile,
  updateCollectionImageCloudinary,
  deleteProductImageCloudinary,
  showImages
} = require('../controllers');

const { validateProductExistByID } = require('../helpers');

const {
  validateFields,
  validateJWT,
  validateUpdaloadFile,
  isAdminRole
} = require('../middlewares');

const router = Router();

router.post('/', [validateUpdaloadFile, validateJWT], uploadFile);

router.put(
  '/:collection/:id',
  [
    validateUpdaloadFile,
    validateJWT,
    check('id', 'El id no es válido').isMongoId(),
    check('collection')
      .isIn(['products', 'users'])
      .withMessage('Solo se admiten las colecciones products, users'),
    validateFields
  ],
  updateCollectionImageCloudinary
);

router.put(
  '/products/remove/:id',
  [
    validateJWT,
    isAdminRole,
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateProductExistByID),
    check('fileName', 'El nombre del archivo no es válido').not().isEmpty(),
    validateFields
  ],
  deleteProductImageCloudinary
);


router.get('/:collection/:id', [
  validateJWT,
  check('id', 'El id no es válido').isMongoId(),
  check('collection')
    .isIn(['products', 'users'])
    .withMessage('Solo se admiten las colecciones products, users'),
  validateFields
], showImages);

module.exports = router;
