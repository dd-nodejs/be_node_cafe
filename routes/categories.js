const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, isAdminRole } = require('../middlewares');
const {
  validateCategoryExistByID,
  validateUniqueCategory
} = require('../helpers');

const {
  createCategory,
  getCategories,
  getCategoryByID,
  updateCategory,
  deleteCategory
} = require('../controllers');

const router = Router();

router.get('/', getCategories);

router.get(
  '/:id',
  [
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateCategoryExistByID),
    validateFields
  ],
  getCategoryByID
);

router.post(
  '/',
  [
    validateJWT,
    check('name', 'El nombre la categoría no es válido').not().isEmpty(),
    check('name').custom(validateUniqueCategory),
    validateFields
  ],
  createCategory
);

router.put(
  '/:id',
  [
    validateJWT,
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateCategoryExistByID),
    check('name', 'El nombre de la categoria no es válido')
      .not()
      .isEmpty()
      .optional({
        nullable: false
      }),
    check('name').custom(validateUniqueCategory),
    validateFields
  ],
  updateCategory
);

router.delete(
  '/:id',
  [
    validateJWT,
    isAdminRole,
    check('id', 'El id no es válido').isMongoId(),
    check('id').custom(validateCategoryExistByID),
    validateFields
  ],
  deleteCategory
);

module.exports = router;
